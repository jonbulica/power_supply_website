<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'lRhmhcmHm+O>>%(k}nTW(SG+zYg sVT7Bkv`1x[OYF~Y{c|):Y7Mf*%Q`/jcGH$|');
define('SECURE_AUTH_KEY',  '!Q-z_d8lzGaWbBDPE]s>p;B2AZuS|Y^U<1!HLm)m,)fhi^&qaHrUO#V!N&:VSCk7');
define('LOGGED_IN_KEY',    '~=56Yb~H:8.lO `pY3#I9ZV$oGLTNQx_*l&jC(EKaT?l`h%`p3T5j1Ib$RG9l(()');
define('NONCE_KEY',        ',A;O6$~i)}yoRZ_xYOh#[ *O9YQ=}by0eno#l79X]vI3^{&}@]tCM@,?/`6qh@e3');
define('AUTH_SALT',        'v)Kx%gj@TdQFiFP,^I~[^#m5Rex+gKkMVPQ,B]-;CGg:l|4&xEeEMW1SN$v6al59');
define('SECURE_AUTH_SALT', 'i[hufrB86`vFAP+@#Bc.r7FU*HN fSj:tiJ;2_ZYrQ_@NH[/.sbdS*!1T;!G8EE[');
define('LOGGED_IN_SALT',   ':BH 3<:7]Km*@1mBr/S+:JU;L4V|i]K6}kxuHMRC:Z*%IR.*[sv-lf^Q0{Jamj9z');
define('NONCE_SALT',       'WN)H5vz.P}] ~njDNc>-}wVBc2|#&Mxj!}+z55jwdbE_P| z:jtIm+cTJ8gdfN}[');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
